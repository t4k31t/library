<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240614223929 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE authors (
            guid UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), 
            author_name VARCHAR NOT NULL, 
            book_list INT DEFAULT NULL,  
            created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
            updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
        )');

        $this->addSql('CREATE TABLE books (
            guid UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), 
            book_title VARCHAR NOT NULL, 
            description TEXT NOT NULL, 
            publish_date DATE NOT NULL, 
            created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
            updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
        )');

        $this->addSql('CREATE TABLE users (
            guid UUID NOT NULL PRIMARY KEY DEFAULT uuid_generate_v4(), 
            user_name VARCHAR NOT NULL, 
            email VARCHAR DEFAULT NULL, 
            password VARCHAR default NULL,
            token INT DEFAULT NULL,
            verified BOOLEAN DEFAULT false,
            created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, 
            updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL
        )');

        $this->addSql('CREATE UNIQUE INDEX UNIQ_8E0C2A51BACCF6D9 ON authors (author_name)');
        $this->addSql('CREATE TABLE authors_books (
            author_id UUID NOT NULL, FOREIGN KEY (author_id) REFERENCES authors(guid) NOT DEFERRABLE INITIALLY IMMEDIATE,
            book_id UUID NOT NULL, FOREIGN KEY (book_id) REFERENCES books(guid) NOT DEFERRABLE INITIALLY IMMEDIATE 
        )');
        $this->addSql('CREATE INDEX IDX_2DFDA3CBF675F31B ON authors_books (author_id)');
        $this->addSql('CREATE INDEX IDX_2DFDA3CB16A2B381 ON authors_books (book_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E924A232CF ON users (user_name)');
        $this->addSql('ALTER TABLE authors_books ADD CONSTRAINT FK_2DFDA3CBF675F31B FOREIGN KEY (author_id) REFERENCES authors (guid) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE authors_books ADD CONSTRAINT FK_2DFDA3CB16A2B381 FOREIGN KEY (book_id) REFERENCES books (guid) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE authors_books DROP CONSTRAINT FK_2DFDA3CBF675F31B');
        $this->addSql('ALTER TABLE authors_books DROP CONSTRAINT FK_2DFDA3CB16A2B381');
        $this->addSql('DROP TABLE authors_books');
        $this->addSql('DROP TABLE authors');
        $this->addSql('DROP TABLE books');
        $this->addSql('DROP TABLE users');
    }
}
