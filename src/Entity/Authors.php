<?php

namespace App\Entity;

use App\Repository\AuthorsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\CustomIdGenerator;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\InverseJoinColumn;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\ManyToMany;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;

#[ORM\Entity(repositoryClass: AuthorsRepository::class)]
#[ORM\Table(name: 'authors')]
class Authors
{
    #[Id]
    #[GeneratedValue(strategy: 'CUSTOM')]
    #[CustomIdGenerator(class: UuidGenerator::class)]
    #[Column(type: Types::GUID, unique: true, nullable: false)]
    private string $guid;
    #[Column(type: 'string', unique: true, nullable: false)]
    private string $authorName;

    /** Owning Side */
    #[ManyToMany(targetEntity: Books::class, inversedBy: 'authors')]
    #[JoinTable(name: 'authors_books')]
    #[JoinColumn(name: 'author_id', referencedColumnName: 'guid')]
    #[InverseJoinColumn(name: 'book_id', referencedColumnName: 'guid')]
    private Collection $books;
    #[Column(type: 'integer', nullable: false)]
    private int $bookList;

    #[Column(type: 'datetime')]
    private ?\DateTime $createdAt = null;
    #[Column(type: 'datetime', nullable: false)]
    private \DateTime $updatedAt;

    public function getGuid(): string
    {
        return $this->guid;
    }

    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getAuthorName(): string
    {
        return $this->authorName;
    }

    public function setAuthorName(string $authorName): self
    {
        $this->authorName = $authorName;

        return $this;
    }

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function addAuthors(Authors $books): self
    {
        if (!$this->books->contains($books)) {
            $this->books[] = $books;
            $books->addAuthors($this);
        }

        return $this;
    }

    public function removeAuthors(Authors $books): self
    {
        if ($this->books->removeElement($books)) {
            $books->removeAuthors($this);
        }

        return $this;
    }

    public function CountBooks(): int
    {
        return $this->books->count();
    }

    public function getBooksList(): int
    {
        return $this->bookList;
    }
    public function setBooksList(int $bookList): self
    {
        $this->bookList = $bookList;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    #[ORM\PreFlush]
    public function preFlush(): void
    {
        if (null === $this->createdAt) {
            $this->createdAt = new \DateTime();
        }
        $this->updatedAt = new \DateTime();
    }
}
